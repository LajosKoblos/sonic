﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileModder
{
    public class Logger
    {
        private StreamWriter writer;
        private string fileName;

        public string FileName
        {
            get 
            {
                return fileName;
            }
            private set
            {
                fileName = value + ".log";
            } 
        }        

        public Logger(string fileName)
        {
            FileName = fileName;
            this.writer = File.AppendText(FileName);
        }

        public string Log(string msg)
        {
            string log = String.Format("{0}", msg);
            writer.WriteLine(log);
            return log;
        }

        public string Log(string msg, int val)
        {
            string log = String.Format("{0}: {1}", msg, val);
            writer.WriteLine(log);
            return log;
        }

        public string Log(int position, string inpCh, string outCh)
        {
            string log = String.Format("{0};{1};{2}", position, inpCh, outCh);
            writer.WriteLine(log);
            return log;
        }

        public string Log(int position, int quantity)
        {
            string log = String.Format("{0}; {1}", position, quantity);
            writer.WriteLine(log);
            return log;
        }

        public string LogInfo(int rowNumber, DateTime startTime, DateTime endTime)
        {
            string log = String.Format("Rownumber: {0}; Start time: {1}; End time: {2}", rowNumber.ToString(), startTime.ToString(), endTime.ToString());
            writer.WriteLine(log);
            return log;
        }

        public void Close()
        {
            writer.Close();
        }
    }
}
