﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileModder
{
    class Modder
    {
        private FileInfo fi;
        private string outDir;
        private DateTime startTime;
        private DateTime endTime;

        public Modder(string fileName, string outDir)
        {
            fi = new FileInfo(fileName);
            this.outDir = outDir;
            startTime = DateTime.Now;
        }

        public void AddCharToRow(char ch, int treshold)
        {
            using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.Default))
            {
                using (StreamWriter sw = File.CreateText(String.Format("{0}\\{1}", outDir, fi.Name)))
                {
                    Logger lgr = new Logger(String.Format("{0}\\{1}", fi.DirectoryName, Path.GetFileNameWithoutExtension(fi.FullName)));
                    StringBuilder sb = new StringBuilder(10000);
                    int rowCounter = 0;

                    while (sr.Peek() != -1)
                    {
                        sb.Append(sr.ReadLine());
                        rowCounter++;
                        int charCounter = sb.ToString().Count(x => x == ch);
                        int ctr = 0;
                        bool isNeedAppend = treshold > charCounter;

                        while (treshold > charCounter)
                        {
                            sb.Append(ch);
                            charCounter++;
                            ctr++;
                        }

                        if (isNeedAppend)
                        {
                            lgr.Log(rowCounter, ctr);
                        }

                        sw.WriteLine(sb.ToString());

                        sb.Clear();
                    }

                    endTime = DateTime.Now;
                    lgr.LogInfo(rowCounter, startTime, endTime);
                    lgr.Close();
                    Console.WriteLine("Enjoy! Rownumber: {0}; Start time: {1}; End time: {2}", rowCounter.ToString(), startTime.ToString(), endTime.ToString());
                }
            }
        }

        public void ReplaceChars()
        {
            int rowCounter = 1;
            Console.Clear();
            Console.WriteLine(fi.FullName);
            StringBuilder sb = new StringBuilder(10000);
            using (BinaryReader br = new BinaryReader(fi.OpenRead(), Encoding.Default))
            {
                Logger logger = new Logger(String.Format("{0}\\{1}", fi.DirectoryName, Path.GetFileNameWithoutExtension(fi.FullName)));
                char prevCh = ' ';
                Regex rgxCR = new Regex("\r[^\n]");
                Regex rgxLF = new Regex("[^\r]\n");
                using (StreamWriter sw = File.CreateText(String.Format("{0}\\{1}", outDir, fi.Name)))
                {
                    while (br.PeekChar() != -1)
                    {
                        char inpCh = br.ReadChar();
                        char outCh = inpCh;
                        switch (inpCh)
                        {
                            case '\x7D':
                                outCh = '\xFC'; // ü 
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x7B':
                                outCh = '\xE4'; // ä
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\xBA':
                                outCh = '\xF6'; // ö
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x21':
                                outCh = '\xDC'; // Ü
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x5C':
                                outCh = '\xD6'; // Ö
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x9B':
                                outCh = '\xC4'; // Ä
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x7E':
                                outCh = '\xDF'; // ß
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            // Control codes
                            case '\x00': // NUL
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x01': // SOH
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x02': // STX
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x03': // ETX
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x04': // EOT
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x05': // ENQ
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x06': // ACK
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x07': // BEL
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x08': // BS
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            //case '\x09': // HT
                            //    outCh = '\x20'; // space
                            //    logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                            //    break;
                            //case '\x0A': // LF
                            //    outCh = '\x20'; // space
                            //    logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                            //    break;
                            case '\x0B': // VT
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x0C': // FF
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            //case '\x0D': // CR
                            //    outCh = '\x20'; // space
                            //    logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                            //    break;
                            case '\x0E': // SO
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x0F': // SI
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x10': // DLE
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x11': // DC1
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x12': // DC2
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x13': // DC3
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x14': // DC4
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x15': // NAK
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x16': // SYN
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x17': // ETB
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x18': // CAN
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x19': // EM
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x1A': // SUB
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x1B': // ESC
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x1C': // FS
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x1D': // GS
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x1E': // RS
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x1F': // US
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                            case '\x7F': // DEL
                                outCh = '\x20'; // space
                                logger.Log(rowCounter, inpCh.ToString(), outCh.ToString());
                                break;
                        }
                        sb.Append(outCh);
                        if (inpCh == '\x0A' && prevCh == '\x0D')
                        {
                            while (rgxCR.IsMatch(sb.ToString()))
                            {
                                int idx = sb.ToString().IndexOf("\r");
                                sb.Remove(idx, 1);
                                logger.Log(rowCounter, "\\r", " ");
                            }
                            while (rgxLF.IsMatch(sb.ToString()))
                            {
                                int idx = sb.ToString().IndexOf("\n");
                                sb.Remove(idx, 1);
                                logger.Log(rowCounter, "\\n", " ");
                            }
                            foreach (char ch in sb.ToString())
                            {
                                sw.Write(ch);
                            }
                            sb.Clear();
                            Console.WriteLine(rowCounter);
                            rowCounter++;
                            Console.SetCursorPosition(0, 1);
                        }
                        prevCh = inpCh;
                    }
                    endTime = DateTime.Now;
                    logger.LogInfo(rowCounter, startTime, endTime);
                    logger.Close();
                }
            }
            Console.WriteLine("Enjoy! Rownumber: {0}; Start time: {1}; End time: {2}", rowCounter.ToString(), startTime.ToString(), endTime.ToString());
        }
    }
}
