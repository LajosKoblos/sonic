﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using FileModder;

namespace RowCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo fi = new FileInfo(args[0]);
            Console.Clear();
            Console.WriteLine(fi.FullName);
            DateTime startTime = DateTime.Now;
            
            using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.Default))
            {                
                Logger lgr = new Logger(String.Format("{0}\\{1}", fi.DirectoryName, Path.GetFileNameWithoutExtension(fi.FullName) + "_tab"));
                int rowNumber = 1;
                int rowCounter105 = 0;
                int maxIdx = 1;
                int maxVal = int.MinValue;
                string line;
                while ((line = sr.ReadLine()) != null)
                {   
                    int quantity = line.Count(x => x == '\x09');
                    switch (quantity)
                    {
                        case 105:
                            lgr.Log(rowNumber, quantity);
                            rowCounter105++;
                            break;
                    }
                    Console.WriteLine("{0}; {1}", rowNumber, quantity);
                    if (quantity > maxVal)
                    {
                        maxIdx = rowNumber;
                        maxVal = quantity;
                        Console.WriteLine("Max tab/row index: {0}, value: {1}", maxIdx, maxVal);
                    }
                    Console.SetCursorPosition(0, 1);
                    rowNumber++;
                }

                Console.WriteLine(lgr.LogInfo(rowCounter105, startTime, DateTime.Now));
                Console.WriteLine(lgr.Log("Max tab/row index:", maxIdx));
                Console.WriteLine(lgr.Log("Max tab/row value:", maxVal));
                lgr.Close();

                Console.ReadKey();
            }
        }
    }
}
